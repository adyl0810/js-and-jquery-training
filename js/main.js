$(() => {
    const strings = [$('#string-3'), $('#string-5'), $('#string-2'), $('#string-6'), $('#string-4'), $('#string-1')]
    $.each(strings, function (index, value) {
        console.log(value.text())
    })

    const elements = $('.element');
    for(let i = 0; i < elements.length; i++) {
        const element = $(elements[i]);
        if (i < 3)
            element.css('color', 'red');
        else
            element.css('color', 'green');
    }

    const tasks = ['Buy lemonade', 'Make toasts', 'Repair car', 'Play games', 'Pet a cat'];
    const todoList = $('#todo-list')
    for(let i = 0; i < tasks.length; i++) {
        const newLi = $('<li class="task"></li>').css('list-style-type', 'none')
        newLi.append(tasks[i])
        todoList.append(newLi)
    }

    const p = document.querySelectorAll('p')
    for (let i = 0; i < p.length; i++) {
        p[i].insertAdjacentHTML('afterend', '<hr/>')
    }

    const newItem = document.createElement("div");
    const newSpan = document.createElement("span");
    newItem.setAttribute("class", "item")
    newSpan.setAttribute("class", "qty")
    const newItemContent = document.createTextNode("Canned Fish ");
    const newSpanContent = document.createTextNode("x 4");
    newItem.appendChild(newItemContent);
    newSpan.appendChild(newSpanContent);
    newItem.appendChild(newSpan);
    const cartItems = document.getElementById("cart-items");
    const items = $('.item')
    for (let i = 0; i < items.length; i++) {
        const item = $(items[i]);
        if (i === 1)
            item.remove()
        if (i === 4)
            cartItems.replaceChild(newItem, cartItems.childNodes[i*2]);
    }

    const container = $('<div>')
    $("body").append(container)
    const div = $('<div>')
    const btn = $('<div>')
    btn.addClass('btn')
    btn.text('Click me')
    container.eq(0).append(btn)
    const modal = $('<div>')
    container.append(modal)
    btn.on('click', () => {
        div.toggleClass('inactive')
        modal.toggleClass('isOpen')
    })
    while (true) {
        const input = prompt("Type something")
        console.log(input)
        if(input === null) break
        const ul = $('<ul>')
        ul.text(input)
        container.eq(0).append(ul)
    }
})